apiVersion: v1
kind: Secret
metadata:
  name: registry-mirror-config
type: Opaque
stringData:
  config.yml: |
    version: 0.1
    log:
      fields:
        service: registry
    storage:
      s3:
        region: us-east-1
        regionendpoint: http://rook-ceph-rgw-generic.rook-ceph.svc:80
        encrypt: false
        secure: true
        v4auth: true
        chunksize: 5242880
        rootdirectory: /
      redirect:
        disable: true
    proxy:
      {{- if .Values.harbor }}
      remoteurl: http://proxy
      {{- else }}
      remoteurl: "{{ .Values.remoteurl }}"
      username: "{{ .Values.username }}"
      password: "{{ .Values.password }}"
      {{- end }}
    http:
      addr: :5000
      headers:
        X-Content-Type-Options: [nosniff]
