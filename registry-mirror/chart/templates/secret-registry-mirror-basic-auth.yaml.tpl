apiVersion: v1
kind: Secret
metadata:
  name: registry-mirror-basic-auth
type: Opaque
stringData:
  auth: "{{ .Values.basic_auth }}"
